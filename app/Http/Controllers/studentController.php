<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\student;


class studentController extends Controller

{
    public function createForm(){
        return view('formdemo');
    }
    public function store(Request $request){
        
        // var_dump($request->image); exit();

        $student =new student;

        $student->first_name=$request->first_name;
        $student->second_name=$request->second_name;
        $student->description=$request->description;
        // var_dump($request->image); exit();
        $name = time().'jpg';

        $request->file('image')->move(public_path('/images/Favourites'),$name);
        
        $student->image= '/images/Favourites/'.$name;
       

        if ($request->fee == 'on'){
            $student->fee="paid";
        }
        else{
            $student->fee='not paid';
        }

        if($student->save()) return redirect('/home');

    // student:: create(Request::all());
    }

    public function getStudents(){


        $student=student::all();
        // var_dump($student); exit();
        
        return view('feedback',['student'=>$student]);
    }
}
