<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
class UserController extends Controller
{
    //
    public function registerUser(Request $request){


        
        $user = new User;
        $user->name = $request->name; 
        $user->email = $request->email; 
        $user->password = bcrypt($request->password);
        
        if($user->save()) return redirect('/home');
        else return redirect()->back(); 

    }

    public function signupview(){
        return view('auth.register');
    }

    public function signinview(){
        return view('auth.login');
    }

    public function getUser(Request $request){
    

        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            // The user is active, not suspended, and exists.
            return redirect ('/home');
        }
        else return redirect()->back();

    }

    public function logout(){

        return redirect('/signin');


    }

    
}
