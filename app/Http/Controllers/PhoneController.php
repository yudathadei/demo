<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Phone;

class PhoneController extends Controller
{
    //
   public function getPhones() {

        $phones = Phone::all();
    
        // var_dump($phones); exit();
    
        // retrieve from database
    
        return view('demo',['phones'=>$phones]);
    }


    public function getSpecificItem($item_id){

        $phone = Phone::find($item_id);
        return view('store2',['phone'=>$phone]);
    }
}
