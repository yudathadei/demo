<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\tv;
use App\brand;

class TvController extends Controller
{
    public function view(){
        
        $tvs= tv::get();
        $brands=brand::get();

        return view('tviewer',['tvs'=>$tvs,'brands'=>$brands]);
    }
}
