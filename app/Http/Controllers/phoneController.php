<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

use App\phone;
use App\brand;
use App\tv;

class phoneController extends Controller
{
    public $cart=3;
    public function add_to_cart(){
        $this->cart+=1;
        var_dump($cart); exit();
    }
    
    public function getPhones(){

        $phones=phone::with('brand')->get();
        $brands=brand::get();
        $tvs= tv::get();

        return view('demo',['phones'=>$phones,'brands'=>$brands,'tvs'=>$tvs]);
    }
    public function fillTable(){

        $phones= phone::with('brand')->get();
        $tvs= tv::get();
        $brands=brand::get();



        return view('/feedback',['phones'=>$phones,'tvs'=>$tvs ,'brands'=>$brands]);
    }

    public function getPhone($brandId,$phoneId){
        $phones = phone::where('brand_id',$brandId)->get();
        $phone = phone::find($phoneId);
        $brands=brand::get();
        $tvs= tv::get();


    
        return view('store',['phones'=>$phones ,'phone'=>$phone,'cart'=>$this->cart,'brands'=>$brands, 'tvs'=>$tvs]);
    }
    public function getBrandPhone($brandId){
        $phones = phone::where('brand_id',$brandId)->get();

        $phone = phone::find($phones[0]->id);
        $brands=brand::get();
        $tvs= tv::get();
    
        return view('store',['phones'=>$phones ,'phone'=>$phone,'cart'=>$this->cart,'brands'=>$brands,'tvs'=>$tvs]);

    }

    public function store(Request $request){

        // var_dump($request->brand_id); exit();
        $data= new phone;

        $data->name=$request->name;
        $data->brand_id=$request->brand_id;
        $data->dimensions=$request->dimensions;
        $data->weight=$request->weight;
        $data->color=$request->color;
        $data->front_camera=$request->front_camera;
        $data->main_camera=$request->main_camera;
        $data->RAM=$request->RAM;
        $data->storage=$request->storage;
        $data->battery=$request->battery;
        $data->price=$request->price;

        $name=time().'.jpeg';

        $request->file('image')->move(public_path('/images/products/'),$name);

        $data->image='/images/products/'.$name;

        if($data->save()) return redirect ('/phone');
    }
    public function edit($item_id){

        $phone= phone::find($item_id);
        $brands=brand::get();


        return view('/editdb',['phone'=>$phone, 'brands'=>$brands,'tvs'=>$tvs]);
    }
    
    public function update(Request $request ){

        $data= phone::find($request->id);

        // var_dump($data->); exit();
         
        $data->name=$request->name;
        $data->brand_id=$request->brand_id;
        $data->dimensions=$request->dimensions;
        $data->weight=$request->weight;
        $data->color=$request->color;
        $data->front_camera=$request->front_camera;
        $data->main_camera=$request->main_camera;
        $data->RAM=$request->RAM;
        $data->storage=$request->storage;
        $data->battery=$request->battery;
        $data->price=$request->price;

        $name=time().'.jpeg';

        $request->file('image')->move(public_path('/images/products/'),$name);

        $data->image='/images/products/'.$name;

       if ($data->save()) return redirect('/feedback');
    }

    public function delete($item_id){
         
        $data= phone::where('id',$item_id)->delete();

        return redirect('/feedback');
    }

}
