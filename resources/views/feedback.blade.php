@extends('layouts.mainlayout')
@section('content')
<div class="container-fluid">
<div class="row ">
    <div class="col-md-12 feeds">
        <table class="table table-striped table-dark">
            <thead>
                <tr>
                <th scope="col">id</th>
                <th scope="col">name</th>
                <th scope="col">brand</th>
                <th scope="col">color</th>
                <th scope="col">dimensions</th>
                <th scope="col">weight</th>
                <th scope="col">front_camera</th>
                <th scope="col">main_camera</th>
                <th scope="col">RAM</th>
                <th scope="col">storage</th>
                <th scope="col">battery</th>
                <th scope="col">price</th>
                <th scope="col">image</th>
                <th scope="col"> Edit</th>
                <th scope="col">Delete</th>
                </tr>
            </thead>
            <tbody>
                @for($i=0;$i < count($phones);$i++)
                <tr>
                    <td>{{$phones[$i]->id}}</td>
                    <td>{{$phones[$i]->name}}</td>
                    <td>{{$phones[$i]->brand->name}}</td>
                    <td>{{$phones[$i]->color}}</td>
                    <td>{{$phones[$i]->dimensions}}</td>
                    <td>{{$phones[$i]->weight}}</td>
                    <td>{{$phones[$i]->front_camera}}</td>
                    <td>{{$phones[$i]->main_camera}}</td>
                    <td>{{$phones[$i]->RAM}}</td>
                    <td>{{$phones[$i]->storage}}</td>
                    <td>{{$phones[$i]->battery}}</td>
                    <td>{{$phones[$i]->price}}</td>
                    <td><img class="table_img" src="{{$phones[$i]->image}}" alt=""></td>
                    <td><a href="/edit/{{$phones[$i]->id}}"><button>Edit</button></a></td>
                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$phones[$i]->id}}">delete</button></td>
                    <div class="modal fade" id="exampleModal{{$phones[$i]->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">confirm deletion</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            Are you sure ,You want to delete this?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <a href="{{$phones[$i]->id}}"><button type="button" class="btn btn-primary">Delete</button></a>
                            

                        </div>
                        </div>
                    </div>
                    </div>
                </tr>
                @endfor

            </tbody>
        </table>
    </div>
</div>
</div>


@endsection