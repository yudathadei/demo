<div class="row top">
    <div class="col-md-8">
    <nav class="navbar navbar-expand-lg navbar-light bg-light hh">
<a class="navbar-brand" href="#">
    <img src="/images/demo.jpeg" alt="demo logo" width="58" height="30" class="d-inline-block align-top" alt="">
</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          phone
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        @for($i=0;$i< count($brands);$i++)
          <a class="dropdown-item" href="/store/{{$brands[$i]->id}}">{{$brands[$i]->name}}</a>
        @endfor
          
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Tv
        </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          @for($i=0; $i< count($tvs); $i++)

            <a class="dropdown-item" href="/tv">{{$tvs[$i]->name}}</a>

          @endfor
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Laptops
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="#">LG</a>
          <a class="dropdown-item" href="#">Samsung</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="#">Something else here</a>
        </div>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
    </div>

            <div class="col-md-4 icon">
                <ul class="navbar">
                    <a href="/home">
                        <li class="nav-links">
                            <i class="fa fa-home"></i>
                        </li>
                    </a>
                    <a href="/phone">
                        <li class="nav-links">
                            <i class="fa fa-pencil"></i>

                        </li>
                    </a>
                    <a href="/feedback">
                        <li class="nav-links">
                        <i class="fa fa-table"></i>
                        </li>
                    </a>
                    <a href="/logout">
                        <li class="nav-links">
                        <i class="fa fa-sign-out"></i>
                        </li>
                    </a>
              
                </ul>
            </div>
        </div>