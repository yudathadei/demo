@extends('layouts.mainlayout')
@section('content')
<div class="container" style="margin-top:200px">
            <form action="store" method="post" enctype="multipart/form-data">
                <label for="name">Enter name</label>
                <div class="row">
                    <div class="col">
                    <input name="first_name" type="text" class="form-control" placeholder="First name">
                    </div>
                    <div class="col">
                    <input name="second_name" type="text" class="form-control" placeholder="Last name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Desciption</label>
                    <textarea name="description" class="form-control" id="" rows="3"></textarea>
                </div>
                <div class="custom-file">
                    <input name="image" type="file" class="custom-file-input" id="" accept='image/*'>
                    <label class="custom-file-label" for="image">Upload image</label>
                </div>
                <div class="form-check">
                    <input name="fee" class="form-check-input" type="checkbox" id="defaultCheck1">
                    <label class="form-check-label" for="defaultCheck1">
                        Fee Paid
                    </label>
                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="submit"  value="submit">
        </form>
        </div>
@endsection
