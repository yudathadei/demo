@extends('layouts.mainlayout')
@section('content')
<div class="container">
<form class="formBox" action="/update" method="post" enctype="multipart/form-data">
@csrf
  <div class="form-row colspace">
    <div class="col">
      <input  name="name" type="text" class="form-control" value="{{$phone->name}}" >
    </div>
    <div class="col">
      <div class="col form-group">
          <select class="form-control" name="brand_id">
           @for($i=0; $i < count($brands);$i++)
              <option value="{{$brands[$i]->id}}">{{$brands[$i]->name}}</option>
            @endfor
          </select>
      </div>
    </div>
    <div class="col">
      <input  name="color" type="text" class="form-control" value="{{$phone->color}}">
    </div>
</div>
<div class="form-row colspace">
    <div class="col">
      <input  name="dimensions" type="text" class="form-control" value="{{$phone->dimensions}}">
    </div>
    <div class="col">
      <input name="weight" type="text" class="form-control" value="{{$phone->weight}}">
    </div>
    <div class="col">
      <input  name="front_camera" type="text" class="form-control" value="{{$phone->front_camera}}">
    </div>
</div>
<div class="form-row colspace">
    <div class="col">
      <input  name="main_camera" type="text" class="form-control" value="{{$phone->main_camera}}">
    </div>
    <div class="col">
      <input name="storage" type="text" class="form-control" value="{{$phone->storage}}">
    </div>
    <div class="col">
      <input name="RAM" type="text" class="form-control" value="{{$phone->RAM}}">
    </div>
</div>
<div class="form-row colspace">
    <div class="col">
      <input name="battery" type="text" class="form-control" value="{{$phone->battery}}">
    </div>
    <div class="col">
      <input name="price" type="text" class="form-control" value="{{$phone->price}}">
    </div>
    <div class="col custom-file">
    <input name="image" type="file" class="custom-file-input"  accept='image/*'>
    <label class="custom-file-label" for="image">{{$phone->image}}</label>
    </div>
</div>
<div class="form-row colspace">
    <div class="col ">
        <input type="hidden" name="_token" value="{{csrf_token()}}"> 
        <input type="hidden" name="id" value="{{$phone->id}}">   
        <button type="submit" class="btn btn-primary">Update</button>
    </div>
  </div>
</form>
</div>

@endsection