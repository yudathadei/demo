@extends('layouts.mainlayout') 
@section('content')
<div class="container">
    <div class="row box">
            <div class="col-md-1 minpic" style="background-image:url('{{$phone->image}}')"></div>
            <div class="col-md-4 offset-md-1 phone"  style="background-image:url('{{$phone->image}}')"></div>
            <div class="col-md-5 offset-md-1">
                <div class="phoneName">{{$phone->brand->name}} {{$phone->name}}</div>
                    <div class="row " style="background-color:rgb(200,100,50)">
                        <div class="col-md-6 price">{{$phone->price}}<i>Tsh</i></div>
                        <div class="col-md-6">
                            <p>For more description</p>
                            <a href="">click here!</a>
                        </div>
                    </div>
                <p>Specifications:</p>
                <Ul>
                    <li>Dimensions: {{$phone->dimensions}}mm</li>
                    <li>Weight: {{$phone->weight}}gm</li>
                    <li>Colour:{{$phone->color}}</li>
                    <li>RAM: {{$phone->RAM}}GB</li>
                    <li>Storage {{$phone->storage}}GB</li>
                    <li>Front camera: {{$phone->front_camera}}MP</li>
                    <li>Main camera: {{$phone->main_camera}}MP</li>
                    <li>Battery: {{$phone->battery}}mAh</li> 
                </Ul>
                <a href="#" >
                
                    <button type="button" id="cart-button" onclick="addCart()" class="btn btn-primary btn-sm cart_btn" >add to cart <span class="badge badge-light" id="cart-count">0</span>
                    </button>
                </a>
                
            </div>  
    </div>
</div>
<div class="container-fluid">
    <div class="row z">
            <ul class="brand_product_list">
            @for($i=0;$i< count($phones);$i++)
                        <li>
                            <a href="/{{$phones[$i]->brand_id}}/{{$phones[$i]->id}}"><div class='a'><img src="{{$phones [$i]->image}}" alt=""></div></a>
                        </li>
            @endfor

            </ul>
    </div>
</div>
@endsection


