@extends('layouts.mainlayout')

@section('content')
<div class="container">
<form class="formBox" action="save" method="post" enctype="multipart/form-data">
@csrf
  <div class="form-row colspace">
    <div class="col">
      <input  name="name" type="text" class="form-control" placeholder="name" required>
    </div>
    <div class="col">
      <!-- <input  name="brand" type="text" class="form-control" placeholder="brand"> -->
      <div class="col form-group">
          <select class="form-control" name="brand_id">
           @for($i=0; $i < count($brands);$i++)
              <option value="{{$brands[$i]->id}}">{{$brands[$i]->name}}</option>
            @endfor
          </select>
      </div>
    </div>
    <div class="col">
      <input  name="color" type="text" class="form-control" placeholder="color">
    </div>
</div>
<div class="form-row colspace">
    <div class="col">
      <input  name="dimensions" type="text" class="form-control" placeholder="dimensions">
    </div>
    <div class="col">
      <input name="weight" type="text" class="form-control" placeholder="weight">
    </div>
    <div class="col">
      <input  name="front_camera" type="text" class="form-control" placeholder="front_camera">
    </div>
</div>
<div class="form-row colspace">
    <div class="col">
      <input  name="main_camera" type="text" class="form-control" placeholder="main_camera">
    </div>
    <div class="col">
      <input name="storage" type="text" class="form-control" placeholder="storage">
    </div>
    <div class="col">
      <input name="RAM" type="text" class="form-control" placeholder="RAM">
    </div>
</div>
<div class="form-row colspace">
    <div class="col">
      <input name="battery" type="text" class="form-control" placeholder="battery">
    </div>
    <div class="col">
      <input name="price" type="text" class="form-control" placeholder="price">
    </div>
    <div class="col custom-file">
    <input name="image" type="file" class="custom-file-input"  accept='image/*'>
    <label class="custom-file-label" for="image">Upload image</label>
    </div>
</div>
<div class="form-row colspace">
    <div class="col ">
        <input type="hidden" name="_token" value="{{csrf_token()}}">    
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </div>
</form>
</div>

@endsection