@extends('layouts.mainlayout')

@section('content')
<div class="container ">
    <div class="row rltv" >
        <div class="col-md-3">
            <div class="card" style="width: 18rem;">
                <img src="{{$brands[1]->image}}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">Card title</h5>
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.
                    Some quick example text to build on the card title and make up the bulk of the card's content.
                    </p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
        </div>
        

        <div class="col-md-8 offset-md-1">
            <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner this">
                    <div class="carousel-item active" data-interval="10000">
                    <img src="images/dem.jpeg" class="d-block w-100" alt="...">
                    </div>
                    @for($i=0;$i< count($phones); $i++)
                        <div class="carousel-item" data-interval="2000">
                        <img src="{{$phones[$i]->image}}" class="d-block w-100 liveimg" alt="...">
                        </div>
                    @endfor
                    <div class="carousel-item">
                    <img src="images/dem.jpeg" class="d-block w-100" alt="...">
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            
        </div>
    </div>
 
</div>
@endsection
