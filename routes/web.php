<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::post('signup','UserController@registerUser')->name('signup');
Route::get('signup','UserController@signupview');
Route::post('signin','UserController@getUser')->name('signin');
Route::get('signin','UserController@signinview');
Route::get('logout','UserController@logout');


Route::middleware(['auth'])->group(function(){
    Route::get('/home', 'phoneController@getPhones')->name('home');
    Route::get('/store/{brandId}','phoneController@getBrandPhone');
    Route::get('/{brandId}/{phoneId}','phoneController@getPhone');
    Route::get('/cart','phoneController@add_to_cart');
    Route::get('/tv','TvController@view')->name('tv');
    Route::get('/phone','brandcontroller@getBrands');
    Route::get('/feedback','phoneController@fillTable'); 
       
    Route::post('/save','phoneController@store');
    Route::get('/edit/{item_id}','phoneController@edit');
    Route::post('/update','phoneController@update');
    Route::get('/{item_id}','phoneController@delete');



});


// Route::get('/home', 'HomeController@index')->name('home');



